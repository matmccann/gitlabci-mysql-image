# Database dump goes here
Database dumps with the extensions .sh, .sql and .sql.gz will be executed when
building this image. See *Initializing a fresh instance* in the 
[MySQL docker docs](https://hub.docker.com/_/mysql).