# Gitlab-CI MySql Database Dump

Docker-in-docker in gitlab-ci is used to create a mysql image for building and testing in CI pipeline. This configuration automatically dumps your database into the image so that all development and CI for your team have a playground database.

---

### Project Setup for Your Use Case
- setup environment variables in gitlab-ci
- change ```tag``` in gitlab-ci to initialize the gitlab-ci.yml (build and save image) with runner tag
- ssh credentials intact for ```development``` (where you develop) and ```master-db server``` (database.latest.mysql.gz is located here)

## Modifying My gitlab-ci.yml for a pipeline
- you must have these variables in gitlab-ci so that your gitlab-ci.yml can pick them up:
```bash
  variables:
    DEPLOY_KEY: "$DEPLOY_KEY_DEV"
    SERVER_SSH: $SERVER_SHH_MYSQL
    DB_DUMP_SNAPSHOT: $DB_MASTER_SNAPSHOT
    DB_DUMP_MOVE_TO: $DB_DUMP_FOLDER
    DEV_SERVER_PATH_TO_SQLGZ: $SERVER_SHH_MYSQL/$MYSQL_SNAPSHOP

```
### Descriptions
1. DEPLOY_KEY_DEV: this is the basic security setup to access your server with ssh
2. SERVER_SSH: user@server-path.com (AWS e.g. user@ec2-xx-xx-xxx-xx.com)
3. DB_DUMP_SNAPSHOT: user@server-path:/db-dump/database.sql (database-server:/path/to/database.mysql.gz)
4. DB_DUMP_MOVE_TO: folder path location to which database is to be moved.
5. DEV_SERVER_PATH_TO_SQLGZ: move file from development server to gitlab-ci folder called "dbdump" in this project.

### Example in Action
- here we hard-code these values in gitlab-ci:

```bash
  script:
    - ssh "user@ec2-52-xx-xxx-xx.com" sftp user@ec2-53-xx-xxx-xx.com:dbdumps/commerce_live.latest.mysql.gz dbdump/database.latest.sql.gz
    - sftp user@ec2-52-xx-xxx-xx.com:dbdump/database.latest.sql.gz dbdump/
```
- and here values are replaced with variables in gitlab-ci:
```bash
  script:
    - ssh "SERVER_SSH" sftp  DB_DRUMP_SNAPSHOT DB_DUMP_MOVE_TO
    - sftp SERVER_PATH_TO_SQLGZ dbdump/
```

- reference [GitLab CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/) for instructions.
---

# TROUBLESHOOTING
- insert ```tail -f /dev/null``` under ```script``` in gitlab-ci.yml and when it hits that line you can click ```debug``` and view a terminal

---

# CONTRIBUTING TO THE README.md
- here are some things that can help. Feel free to add points here if you think improvements can be made and push your suggestions to matmccann@gmail.com


1. setup image in Dockerhub
2. basic Q&A put into the TROUBLESHOOTING
3. create three scripts to run in ```script: ``` section of gitlab-ci.yml

        #1 local_development.sh
        - this uses docker-compose and assumes that you have a database in the dbdump folder (e.g. dbdump/database.latest.sql)
        
        #2 development.sh
        - this assumes that you have the db snapshot in an s3 bucket and want to pull it into the gitlab pipeline and proceed as normal
        
        #3 production.sh
        - this assumes that you have a development server, master-db server, and you want to create the mysql image for a production devOps setting.

---
