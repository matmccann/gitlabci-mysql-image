FROM mysql:5.6

ENV MYSQL_DATABASE=drupal \
    MYSQL_USER=drupal \
    MYSQL_PASSWORD=drupal \
    MYSQLD_RAM_SIZE=4096 \
    # MYSQL_SQL_TO_RUN='GRANT ALL ON *.* TO "drupal"@"%" IDENTIFIED BY "drupal";'
    MYSQL_SQL_TO_RUN='GRANT ALL ON *.* TO "testrunner"@"%";'

ADD dbdump /docker-entrypoint-initdb.d

EXPOSE 3306